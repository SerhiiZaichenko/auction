﻿using Internet_auction.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Internet_auction.DAL.Context
{
    public class AuctionAppContext : DbContext
    {
        public AuctionAppContext(DbContextOptions<AuctionAppContext> options)
          : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasMany(t => t.BuyedLots)
                .WithOne(g => g.Buyer)
                .HasForeignKey(g => g.BuyerId);
            modelBuilder.Entity<User>().HasMany(t => t.SellingLots)
                .WithOne(g => g.Seller)
                .HasForeignKey(g => g.SellerId).OnDelete(DeleteBehavior.Restrict);
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Lot> Lots { get; set; }
        public DbSet<LotDetails> LotsDetails { get; set; }
        public DbSet<UserProfile> Profiles { get; set; }

    }
}
