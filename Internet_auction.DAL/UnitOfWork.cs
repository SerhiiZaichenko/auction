﻿using Internet_auction.DAL.Interfaces;
using Internet_auction.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Internet_auction.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;
        private ILotRepository _lotRepository;
        private IUserRepository _userRepository;



        public UnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public ILotRepository LotRepository
        {
            get
            {
                if (_lotRepository == null)
                {
                    _lotRepository = new LotRepository(_dbContext);
                }
                return _lotRepository;
            }
        }
        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_dbContext);
                }
                return _userRepository;
            }
        }

        public async Task<int> SaveAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }
    }
}
