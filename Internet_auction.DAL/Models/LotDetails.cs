﻿using Internet_auction.Common.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Internet_auction.DAL.Models
{
    public class LotDetails
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public float CurrentBid { get; set; }

        public string LotState { get; set; } = LotStates.OnAuction;
        [Required]
        public float StartPrice { get; set; }
        [Required]
        public float Increment { get; set; }
        public long LotId { get; set; }
        [ForeignKey("LotId")]
        public Lot Lot { get; set; }
     
    }
}
