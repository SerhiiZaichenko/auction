﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Internet_auction.DAL.Models
{
    public class User
    {
        public long Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool IsBanned { get; set; }

        public UserProfile UserProfile { get; set; } = new UserProfile();
        [InverseProperty("Buyer")]
        public ICollection<Lot> BuyedLots { get; set; }
        [InverseProperty("Seller")]
        public ICollection<Lot> SellingLots { get; set; }
    }
}
