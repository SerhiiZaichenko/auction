﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Internet_auction.DAL.Models
{
    public class Lot
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }        
        [Required]
        public string Description { get; set; }

        public LotDetails LotDetails { get; set; } = new LotDetails();
        public long? BuyerId { get; set; }
        [ForeignKey("BuyerId")]
        public User Buyer { get; set; }
        [Required]
        public long? SellerId { get; set; }
        [ForeignKey("SellerId")]
        public User Seller { get; set; }


    }
}
