﻿using Internet_auction.Common.Constants;
using Internet_auction.Common.Dtos;
using Internet_auction.DAL.Context;
using Internet_auction.DAL.Interfaces;
using Internet_auction.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Internet_auction.DAL.Repositories
{
    public class LotRepository:Repository<Lot>,ILotRepository
    {
        public LotRepository(DbContext context) : base(context) {}

        public async Task<Lot> GetLotWithDetailsByIdAsync(long id)
        {
            return await this.GetByAsync(l => l.Id == id, l => l.LotDetails,l => l.Seller,l=>l.Buyer);
        }
        public async Task<List<Lot>> GetLotsByNameAsync(string name)
        {
            return await this.GetAllByAsync(l => l.Name.Contains(name)&& l.LotDetails.LotState==LotStates.OnAuction,l=>l.LotDetails) ;
        }

        public async Task<Lot> GetSellersLotByNameAsync(long sellerId, string name)
        {
           return await this.GetByAsync(l => l.Name == name, l => l.LotDetails);        
           
        }

        public async Task<List<Lot>> getBuyedUsersLots(long id)
        {
            return await this.GetAllByAsync(l => l.BuyerId == id && l.LotDetails.LotState==LotStates.Selled, l => l.LotDetails); 

        }
    }
}
