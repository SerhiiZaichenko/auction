﻿using Internet_auction.DAL.Context;
using Internet_auction.DAL.Interfaces;
using Internet_auction.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Internet_auction.DAL.Repositories
{
    public class UserRepository:Repository<User>,IUserRepository
    {
        public UserRepository(DbContext context) : base(context) { }

        public async Task<User> GetUserById(long id)
        {
            return await this.GetByAsync(u => u.Id == id, u => u.UserProfile);
        }

        public async Task<User> GetUserByLogin(string login)
        {
          return  await this.GetByAsync(u => u.Login == login);
        }

        public async Task<User> GetUserByLoginPassword(string login, string password)
        {
            return await this.GetByAsync(u => u.Login ==login  && u.Password == password,u=>u.UserProfile);
        }
    }
}
