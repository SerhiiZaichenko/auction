﻿using Internet_auction.DAL.Models;
using Internet_auction.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Internet_auction.DAL.Interfaces
{
    public interface IUserRepository:IRepository<User>
    {
        Task<User> GetUserByLoginPassword(string login, string password);
        Task<User> GetUserByLogin(string login);
        Task<User> GetUserById(long id);

    }
}
