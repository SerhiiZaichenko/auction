﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Internet_auction.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        ILotRepository LotRepository { get; }
        IUserRepository UserRepository { get; }
        Task<int> SaveAsync();
    }
}
