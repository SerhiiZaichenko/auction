﻿using Internet_auction.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Internet_auction.DAL.Interfaces
{
    public interface ILotRepository : IRepository<Lot>
    {
        Task<Lot> GetSellersLotByNameAsync(long sellerId, string name);
        Task<Lot> GetLotWithDetailsByIdAsync(long id);
        Task<List<Lot>> GetLotsByNameAsync(string name);
        Task<List<Lot>> getBuyedUsersLots(long id);
    }
}
