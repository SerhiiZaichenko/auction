﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Internet_auction.DAL.Interfaces
{
    public interface IRepository<T>
    {
        Task<List<T>> GetAllByAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes);

        Task<List<T>> GetAllAsync();

        Task<List<T>> GetAllByAsync(Expression<Func<T, bool>> expression);
        Task<T> GetByAsync(Expression<Func<T, bool>> expression);
        Task<T> GetByAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes);

        Task CreateAsync(T item);

        void Update(T item);

        Task DeleteAsync(Expression<Func<T, bool>> expression);
    }
}
