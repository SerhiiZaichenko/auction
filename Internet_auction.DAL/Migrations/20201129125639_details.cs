﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Internet_auction.DAL.Migrations
{
    public partial class details : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lots_Users_SellerId",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "CurrentPrice",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "Increment",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "StartPrice",
                table: "Lots");

            migrationBuilder.AddColumn<bool>(
                name: "IsBanned",
                table: "Users",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "BuyerId",
                table: "Lots",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LotsDetails",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CurrentBid = table.Column<float>(type: "real", nullable: false),
                    LotState = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StartPrice = table.Column<float>(type: "real", nullable: false),
                    Increment = table.Column<float>(type: "real", nullable: false),
                    LotId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LotsDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LotsDetails_Lots_LotId",
                        column: x => x.LotId,
                        principalTable: "Lots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Profiles",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<long>(type: "bigint", nullable: false),
                    Wallet = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Profiles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Lots_BuyerId",
                table: "Lots",
                column: "BuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_LotsDetails_LotId",
                table: "LotsDetails",
                column: "LotId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_UserId",
                table: "Profiles",
                column: "UserId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_Users_BuyerId",
                table: "Lots",
                column: "BuyerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_Users_SellerId",
                table: "Lots",
                column: "SellerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lots_Users_BuyerId",
                table: "Lots");

            migrationBuilder.DropForeignKey(
                name: "FK_Lots_Users_SellerId",
                table: "Lots");

            migrationBuilder.DropTable(
                name: "LotsDetails");

            migrationBuilder.DropTable(
                name: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Lots_BuyerId",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "IsBanned",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "BuyerId",
                table: "Lots");

            migrationBuilder.AddColumn<float>(
                name: "CurrentPrice",
                table: "Lots",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "Increment",
                table: "Lots",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "StartPrice",
                table: "Lots",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_Users_SellerId",
                table: "Lots",
                column: "SellerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
