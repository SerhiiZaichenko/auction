﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Internet_auction.DAL.Migrations
{
    public partial class lotfixed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lots_Users_SellerId1",
                table: "Lots");

            migrationBuilder.DropIndex(
                name: "IX_Lots_SellerId1",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "SellerId1",
                table: "Lots");

            migrationBuilder.AlterColumn<long>(
                name: "SellerId",
                table: "Lots",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Lots_SellerId",
                table: "Lots",
                column: "SellerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_Users_SellerId",
                table: "Lots",
                column: "SellerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lots_Users_SellerId",
                table: "Lots");

            migrationBuilder.DropIndex(
                name: "IX_Lots_SellerId",
                table: "Lots");

            migrationBuilder.AlterColumn<int>(
                name: "SellerId",
                table: "Lots",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddColumn<long>(
                name: "SellerId1",
                table: "Lots",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Lots_SellerId1",
                table: "Lots",
                column: "SellerId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_Users_SellerId1",
                table: "Lots",
                column: "SellerId1",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
