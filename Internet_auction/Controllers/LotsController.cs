﻿using Internet_auction.BLL.Services.Realizations;
using Internet_auction.BLL.Validation;
using Internet_auction.Common.Constants;
using Internet_auction.Common.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Internet_auction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LotsController : Controller
    {
        readonly ILotService _lotService;
        public LotsController(ILotService lotService)
        {
            _lotService = lotService;
        }

        [HttpPost]
        [Authorize(Roles = Roles.Client)]
        public async Task<ActionResult<LotCreateDto>> Create([FromBody] LotCreateDto lotDto)
        {
            try
            {
                await _lotService.CreateLotAsync(lotDto);
            }
            catch (AuctionException)
            {
                return BadRequest();
            }
            return Ok(lotDto);

        }
        [HttpPost("ban")]
        [Authorize(Roles = Roles.Admin + "," + Roles.Client)]
        public async Task<ActionResult> BanLot([FromBody] int lotId)
        {
            try
            {
                await _lotService.BanLot(lotId);
            }
            catch (AuctionException)
            {
                return BadRequest();
            }
            return Ok();
        }
        [HttpPost("bid")]
        [Authorize(Roles = Roles.Admin+","+Roles.Client)]
        public async Task<ActionResult<BidDto>> DoBid([FromBody] BidDto bid)
        {
            try
            {
                await _lotService.DoBid(bid);
            }
            catch (AuctionException)
            {
                return BadRequest();
            }
            return Ok(bid);
        }
        [HttpPost("sell")]
        [Authorize(Roles = Roles.Admin + "," + Roles.Client)]
        public async Task<ActionResult<SellLotDto>> SellLot([FromBody] SellLotDto lot)
        {
            try
            {
                await _lotService.SellLot(lot);
            }
            catch (AuctionException)
            {
                return BadRequest();
            }
            return Ok(lot);
        }
        [HttpGet]
        [Authorize(Roles = Roles.Admin + "," + Roles.Client)]
        public async Task<ActionResult<List<LotDto>>> GetLotsAsync()
        {
            return Ok(await _lotService.GetAllLotsAsync());
        }
        [HttpGet("{id}")]
        [Authorize(Roles = Roles.Admin + "," + Roles.Client)]
        public async Task<ActionResult<LotDetailedDto>> GetLotByIdAsync(int id)
            {
            LotDetailedDto lot;
            try
            {
                lot = await _lotService.GetLotByIdAsync(id);
            }
            catch (AuctionException)
            {
                return NotFound();
            }
            return Ok(lot);
        }
        [HttpGet("byName")]
        [Authorize(Roles = Roles.Admin + "," + Roles.Client)]
        public async Task<ActionResult<List<LotDto>>> GetLotsByName([FromQuery] string name)
        {           
            try
            {
                if (name==null)
                    return Ok(await _lotService.GetAllLotsAsync());
                else
                    return Ok(await _lotService.GetLotsByName(name));                
            }
            catch (AuctionException)
            {
                return NotFound();
            }
          

        }
    }
}
