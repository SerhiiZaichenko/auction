﻿using Internet_auction.BLL.Services;
using Internet_auction.BLL.Validation;
using Internet_auction.Common.Constants;
using Internet_auction.Common.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Internet_auction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Authorize(Roles = Roles.Admin)]
        public async Task<ActionResult<List<UserForListDto>>> GetUsersAsync()
        {
            try
            {
                return Ok(await _userService.GetAllUsersAsync());
            }
            catch (AuctionException)
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.Admin)]
        public async Task<ActionResult> BanUser(long id)
        {
            try
            {
                await _userService.BanUser(id);
                return Ok();
            }
            catch (AuctionException)
            {
                return BadRequest();
            }
        }
        [HttpPost("deposit")]
        [Authorize(Roles = Roles.Admin + "," + Roles.Client)]
        public async Task<ActionResult> MakeDeposit([FromBody] DepositDto deposit)
        {
            try
            {
                await _userService.MakeDeposit(deposit.UserId, deposit.Sum);
                return Ok();
            }
            catch (AuctionException)
            {
                return BadRequest();
            }
        }
        [HttpGet("{id}/lots")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<LotDto>>> GetUserLots(long id)
        {
            try
            {
                return Ok(await _userService.getBuyedUsersLots(id));
            }
            catch (AuctionException)
            {
                return NotFound();
            }
        }
    }
}
