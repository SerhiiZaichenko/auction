﻿using Internet_auction.BLL.Services;
using Internet_auction.Common.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Internet_auction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : Controller
    {
        readonly IAuthenticationService _authenticationService;
        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<ActionResult> Register([FromBody] UserDto userDto)
        {
            try
            {
                await _authenticationService.RegisterAsync(userDto);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<UserLoggedDto>> Login(UserDto userAuthDto)
        {
            try
            {
                return Ok(await _authenticationService.Authenticate(userAuthDto));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
