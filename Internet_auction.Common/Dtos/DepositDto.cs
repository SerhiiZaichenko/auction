﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Internet_auction.Common.Dtos
{
    public class DepositDto
    {
        public long UserId { get; set; }
        public float Sum { get; set; }
    }
}
