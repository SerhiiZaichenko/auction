﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Internet_auction.Common.Dtos
{
    public class BidDto
    {       
        public long LotId { get; set; }
        public long BuyerId { get; set; }
        public float Bid { get; set; }
    }
}
