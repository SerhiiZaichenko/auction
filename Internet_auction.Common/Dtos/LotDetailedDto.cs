﻿namespace Internet_auction.Common.Dtos
{
    public class LotDetailedDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public float StartPrice { get; set; }
        public float CurrentPrice { get; set; }
        public string Description { get; set; }
        public string LotState { get; set; }
        public float Increment { get; set; }
        public string SellerName { get; set; }
    }
}
