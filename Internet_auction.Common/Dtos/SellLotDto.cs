﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Internet_auction.Common.Dtos
{
    public class SellLotDto
    {
        public long SellerId { get; set; }
        public long LotId { get; set; }
    }
}
