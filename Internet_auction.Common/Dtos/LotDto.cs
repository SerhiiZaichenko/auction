﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Internet_auction.Common.Dtos
{
    public class LotDto
    {
        public long Id { get; set; }
        public string Name { get; set; }      
        public string Description { get; set; }      
      
    }
}
