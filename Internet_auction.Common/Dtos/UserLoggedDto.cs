﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Internet_auction.Common.Dtos
{
    public class UserLoggedDto
    {
        public long UserId { get; set; }        
        public string Login { get; set; }
        public string Role { get; set; }
        public float Wallet { get; set; }
        public string Token { get; set; }

    }
}
