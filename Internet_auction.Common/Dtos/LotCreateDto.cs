﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Internet_auction.Common.Dtos
{
    public class LotCreateDto
    {
        public string Name { get; set; }       
        public float StartPrice { get; set; }        
        public string Description { get; set; }      
        public float Increment { get; set; }
        public long SellerId { get; set; }
    }
}
