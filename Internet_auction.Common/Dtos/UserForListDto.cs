﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Internet_auction.Common.Dtos
{
    public class UserForListDto
    {
        public long Id { get; set; }
        public string Login { get; set; }
    }
}
