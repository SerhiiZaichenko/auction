﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Internet_auction.Common.Dtos
{
    public class UserDto
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
