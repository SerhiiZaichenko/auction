﻿using AutoMapper;
using Internet_auction.Common.Constants;
using Internet_auction.Common.Dtos;
using Internet_auction.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Internet_auction.BLL
{
    public class AutomapperProfile:Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Lot, LotDetailedDto>()
                .ForMember(p => p.SellerName, l => l.MapFrom(lot => lot.Seller.Login))
                .IncludeMembers(l => l.LotDetails);
            CreateMap<LotDetails, LotDetailedDto>()
                .ForMember(p => p.CurrentPrice, l => l.MapFrom(lot => lot.CurrentBid));
            CreateMap<Lot, LotDto>();
            CreateMap<Lot, LotCreateDto>()
                .IncludeMembers(l=>l.LotDetails).ReverseMap();
            CreateMap<LotDetails, LotCreateDto>()
                .ForMember(p => p.StartPrice, opt => opt.MapFrom(lot => lot.CurrentBid))
                .ReverseMap();                
            CreateMap<User, UserDto>();
            CreateMap<User, UserForListDto>();
            CreateMap<UserDto, User>()
                .ForMember(p => p.Role, opt=>opt.MapFrom(dto=>Roles.Client));
        }
    }
}
