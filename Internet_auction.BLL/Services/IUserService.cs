﻿using Internet_auction.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Internet_auction.BLL.Services
{
    public interface IUserService
    {
        Task<List<UserForListDto>> GetAllUsersAsync();
        Task BanUser(long id);
        Task MakeDeposit(long id, float sum);
        Task<IEnumerable<LotDto>> getBuyedUsersLots(long id);
    }
}
