﻿using Internet_auction.Common.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Internet_auction.BLL.Services.Realizations
{
    public interface ILotService
    {
        Task<List<LotDto>> GetAllLotsAsync();
        Task<LotDetailedDto> GetLotByIdAsync(long id);
        Task CreateLotAsync(LotCreateDto lotDto);
        Task DoBid(BidDto bidDto);
        Task SellLot(SellLotDto sellLotDto);
        Task<List<LotDto>> GetLotsByName(string name);
        Task BanLot(long lotId);
    }
}
