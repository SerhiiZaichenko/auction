﻿using Internet_auction.Common.Dtos;
using System.Threading.Tasks;

namespace Internet_auction.BLL.Services
{
    public interface IAuthenticationService
    {
        Task RegisterAsync(UserDto userDto);

        Task<UserLoggedDto> Authenticate(UserDto userDto);

    }
}
