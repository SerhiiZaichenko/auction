﻿using AutoMapper;
using Internet_auction.BLL.Validation;
using Internet_auction.Common.Dtos;
using Internet_auction.Common.Helpers;
using Internet_auction.DAL.Interfaces;
using Internet_auction.DAL.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Internet_auction.BLL.Services.Realizations
{
    public class AuthenticationService : IAuthenticationService
    {

        private readonly AppSettings _appSettings;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public AuthenticationService(IOptions<AppSettings> appSettings, IMapper mapper, IUnitOfWork unitOfWork)
        {

            _appSettings = appSettings.Value;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        /// <summary>
        /// Authenticate user, creates token if login succeed
        /// </summary>
        /// <param name="userDto">Data for login</param>
        /// <returns>User data with token</returns>
        public async Task<UserLoggedDto> Authenticate(UserDto userDto)
        {
            var user = await _unitOfWork.UserRepository.GetUserByLoginPassword(userDto.Login, userDto.Password);
            if (user == null || user.IsBanned)
                throw new AuctionException();

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);


            UserLoggedDto userLoggedDto = new UserLoggedDto
            {
                UserId = user.Id,
                Login = user.Login,
                Role = user.Role,
                Wallet = user.UserProfile.Wallet,
                Token = tokenHandler.WriteToken(token),

            };

            return userLoggedDto;

        }
        public async Task RegisterAsync(UserDto userDto)
        {
            var user = await _unitOfWork.UserRepository.GetUserByLogin(userDto.Login);
            if (user != null)
            {
                throw new AuctionException();
            }

            await _unitOfWork.UserRepository.CreateAsync(_mapper.Map<UserDto, User>(userDto));
            await _unitOfWork.SaveAsync();

        }
    }
}
