﻿using AutoMapper;
using Internet_auction.BLL.Validation;
using Internet_auction.Common.Constants;
using Internet_auction.Common.Dtos;
using Internet_auction.DAL.Interfaces;
using Internet_auction.DAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Internet_auction.BLL.Services.Realizations
{
    public class LotService : ILotService
    {

        readonly IMapper _mapper;
        readonly IUnitOfWork _unitOfWork;
        public LotService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;

        }
        /// <summary>
        /// Soft delete lot if exists and on auction, returns money to buyer
        /// </summary>
        /// <param name="lotId">Lot identifier</param>
        /// <returns></returns>
        public async Task BanLot(long lotId)
        {
            var lot = await _unitOfWork.LotRepository.GetLotWithDetailsByIdAsync(lotId);
            if (lot == null)
                throw new AuctionException();
            if (lot.LotDetails.LotState != LotStates.OnAuction)
                throw new AuctionException();
            if (lot.BuyerId != null)
            {
                var user = await _unitOfWork.UserRepository.GetUserById(lot.BuyerId ?? default(long));
                if (user != null)
                {
                    user.UserProfile.Wallet += lot.LotDetails.CurrentBid;
                    _unitOfWork.UserRepository.Update(user);
                }
            }
            lot.LotDetails.LotState = LotStates.Banned;
            _unitOfWork.LotRepository.Update(lot);
            await _unitOfWork.SaveAsync();

        }

        public async Task CreateLotAsync(LotCreateDto lotDto)
        {
            if (lotDto.Description == null
                || lotDto.Name == null)
                throw new AuctionException();
            var lot = await _unitOfWork.LotRepository.GetSellersLotByNameAsync(lotDto.SellerId, lotDto.Name);
            if (lot != null)
                throw new AuctionException();
            Lot a = _mapper.Map<LotCreateDto, Lot>(lotDto);
            await _unitOfWork.LotRepository.CreateAsync(a);
            await _unitOfWork.SaveAsync();
        }
        /// <summary>
        /// Set new buyer and price,
        /// returns money to previous buyer,
        /// take money from new buyer
        /// </summary>
        /// <param name="bidDto">Bid data</param>
        /// <returns></returns>
        public async Task DoBid(BidDto bidDto)
        {
            var buyer = await _unitOfWork.UserRepository.GetUserById(bidDto.BuyerId);
            var lot = await _unitOfWork.LotRepository.GetLotWithDetailsByIdAsync(bidDto.LotId);
            if (lot == null || buyer == null)
                throw new AuctionException();
            if (buyer.IsBanned)
                throw new AuctionException();
            if (lot.LotDetails.LotState != LotStates.OnAuction
                || lot.LotDetails.CurrentBid + lot.LotDetails.Increment > bidDto.Bid
                || buyer.UserProfile.Wallet < bidDto.Bid
                || buyer.Id == lot.SellerId)
                throw new AuctionException();
            var prevBidder = lot.Buyer;
            if (prevBidder != null)
            {
                prevBidder.UserProfile.Wallet += lot.LotDetails.CurrentBid;
                _unitOfWork.UserRepository.Update(prevBidder);
            }
            buyer.UserProfile.Wallet -= bidDto.Bid;
            lot.Buyer = buyer;
            lot.LotDetails.CurrentBid = bidDto.Bid;
            _unitOfWork.UserRepository.Update(buyer);
            _unitOfWork.LotRepository.Update(lot);
            await _unitOfWork.SaveAsync();

        }

        public async Task<List<LotDto>> GetAllLotsAsync()
        {
            var lots = await _unitOfWork.LotRepository
                .GetAllByAsync(l => l.LotDetails.LotState == LotStates.OnAuction, l => l.LotDetails);
            return lots.Select(l => _mapper.Map<Lot, LotDto>(l)).ToList();
        }

        public async Task<LotDetailedDto> GetLotByIdAsync(long id)
        {
            var lot = await _unitOfWork.LotRepository.GetLotWithDetailsByIdAsync(id);
            if (lot == null)
                throw new AuctionException();
            return _mapper.Map<Lot, LotDetailedDto>(lot);
        }

        public async Task<List<LotDto>> GetLotsByName(string name)
        {
            var lots = await _unitOfWork.LotRepository.GetLotsByNameAsync(name);
            if (lots == null)
                throw new AuctionException();
            return lots.Select(l => _mapper.Map<Lot, LotDto>(l)).ToList();
        }
        /// <summary>
        /// Updates wallet state of buyer and seller,
        /// set lot to selled
        /// </summary>
        /// <param name="sellLotDto">Lot and seller data</param>
        /// <returns></returns>
        public async Task SellLot(SellLotDto sellLotDto)
        {
            var seller = await _unitOfWork.UserRepository.GetUserById(sellLotDto.SellerId);
            var lot = await _unitOfWork.LotRepository.GetLotWithDetailsByIdAsync(sellLotDto.LotId);
            if (lot == null || seller == null)
                throw new AuctionException();
            if (lot.SellerId != seller.Id
                || lot.Buyer == null
                || lot.LotDetails.LotState != LotStates.OnAuction
                )
                throw new AuctionException();
            var buyer = lot.Buyer;
            buyer.BuyedLots.Add(lot);
            lot.LotDetails.LotState = LotStates.Selled;
            seller.UserProfile.Wallet += lot.LotDetails.CurrentBid;
            _unitOfWork.UserRepository.Update(buyer);
            _unitOfWork.LotRepository.Update(lot);
            _unitOfWork.UserRepository.Update(seller);
            await _unitOfWork.SaveAsync();
        }
    }
}
