﻿using AutoMapper;
using Internet_auction.BLL.Validation;
using Internet_auction.Common.Constants;
using Internet_auction.Common.Dtos;
using Internet_auction.DAL.Interfaces;
using Internet_auction.DAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Internet_auction.BLL.Services.Realizations
{
    public class UserService : IUserService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        /// <summary>
        /// Check if user exists, than soft delete him
        /// </summary>
        /// <param name="id">User identificator</param>
        /// <returns></returns>
        public async Task BanUser(long id)
        {
            var user = await _unitOfWork.UserRepository.GetUserById(id);
            if (user == null)
                throw new AuctionException();
            if (user.IsBanned)
                throw new AuctionException();
            if (user.Role != Roles.Client)
                throw new AuctionException();
            user.IsBanned = true;
            _unitOfWork.UserRepository.Update(user);
            await _unitOfWork.SaveAsync();

        }

        public async Task<List<UserForListDto>> GetAllUsersAsync()
        {
            var users = await _unitOfWork.UserRepository.GetAllAsync();
            return users.Select(u => _mapper.Map<User, UserForListDto>(u)).ToList();
        }
        /// <summary>
        /// Returns lots buyed by user with id, if user exists
        /// </summary>
        /// <param name="id">User identificator</param>
        /// <returns>Lots</returns>
        public async Task<IEnumerable<LotDto>> getBuyedUsersLots(long id)
        {
            var user = await _unitOfWork.UserRepository.GetUserById(id);
            if (user == null)
                throw new AuctionException();
            var lots = await _unitOfWork.LotRepository.getBuyedUsersLots(id);
            if (lots == null)
                throw new AuctionException();
            return lots.Select(l => _mapper.Map<Lot, LotDto>(l));
        }
        /// <summary>
        /// Add sum to wallet of user with id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sum"></param>
        /// <returns></returns>
        public async Task MakeDeposit(long id, float sum)
        {
            var user = await _unitOfWork.UserRepository.GetUserById(id);
            if (user == null)
                throw new AuctionException();
            user.UserProfile.Wallet += sum;
            _unitOfWork.UserRepository.Update(user);
            await _unitOfWork.SaveAsync();
        }
    }
}
